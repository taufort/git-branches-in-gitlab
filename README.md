# git-branches-in-gitlab

A little project to show simple GitLab use cases with GitLab environments

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
